package com.example.demo.api;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

@Service
public class MovieService {

    @Value("${api.key}")
    private String apiKey;
    RestTemplate restTemplate = new RestTemplate();


    public Movie getMovieByTitle(String Title) throws JSONException, IOException, InterruptedException {
        //for titles with multiple words
        String title = Title.replaceAll("\\s","%20");
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create("https://imdb8.p.rapidapi.com/title/find?q=" + title))
                .header("X-RapidAPI-Key", apiKey)
                .header("X-RapidAPI-Host", "imdb8.p.rapidapi.com")
                .method("GET", HttpRequest.BodyPublishers.noBody())
                .build();
        HttpResponse<String> response = HttpClient.newHttpClient().send(request, HttpResponse.BodyHandlers.ofString());
        System.out.println(response.body());

            JSONObject json = new JSONObject(response.body());
            JSONArray results = json.getJSONArray("results");
            JSONObject movieJson = results.getJSONObject(0);

            //to access the array of actor names
            JSONArray leads = movieJson.getJSONArray("principals");
            JSONObject lead = leads.getJSONObject(0);


            Movie movie = new Movie();
            movie.setTitle(movieJson.getString("title"));
            movie.setYear(movieJson.getInt("year"));
            movie.setRunningTimeInMinutes(movieJson.getInt("runningTimeInMinutes"));
            movie.setLeadActor(lead.getString("legacyNameText"));
        return movie;
    }
}


