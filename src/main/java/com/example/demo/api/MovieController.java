package com.example.demo.api;

import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

@RestController
public class MovieController {
    @Autowired
    private MovieService movieservise;

    @GetMapping("/movie")
    public ResponseEntity<Movie> getMovie(@RequestParam(value = "title") String title) throws JSONException, IOException, InterruptedException {
        Movie movie = movieservise.getMovieByTitle(title);
        return ResponseEntity.ok(movie);
    }
}
