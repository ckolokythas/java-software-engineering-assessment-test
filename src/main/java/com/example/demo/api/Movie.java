package com.example.demo.api;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Movie {
    private String title;
    private int year;
    private int runningTimeInMinutes;
    private String leadActor;
}
